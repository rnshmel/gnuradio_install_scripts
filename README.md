Installation bash script:

- Builds GNUradio from source
- Builds UHD from source
- Installs inpsectrum and GQRX
- Builds gr-eventstream and gr-osmosdr

to run, clone repo and run: "sudo ./installer.sh"

Also included are two FM radio GRC projects to test out the installation
- fm_radio_usrp.grc should be run if you are using a Ettus radio
- fm_radio_hackrf.grc should be run if you are using a HackRF