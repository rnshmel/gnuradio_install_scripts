#! /bin/sh

UHDV="v3.14.1.1"
GNURADV="v3.7.13.5"
USRNM=$(logname)
IPATH=$HOME

echo "STARTING INSTALL SCRIPT"
echo "============================="
echo "username: $USRNM"
echo "install path: $IPATH\n"

echo "UPDATING UBUNTU"
echo "=============================\n"
sudo apt-get update

echo "INSTALLING DEPENDS"
echo "=============================\n"
sudo apt-get -y install git swig cmake doxygen build-essential libboost-all-dev libtool libusb-1.0-0 libusb-1.0-0-dev libudev-dev libncurses5-dev libfftw3-bin libfftw3-dev libfftw3-doc libcppunit-1.14-0 libcppunit-dev libcppunit-doc ncurses-bin cpufrequtils python-numpy python-numpy-doc python-numpy-dbg python-scipy python-docutils qt4-bin-dbg qt4-default qt4-doc libqt4-dev libqt4-dev-bin python-qt4 python-qt4-dbg python-qt4-dev python-qt4-doc python-qt4-doc libqwt6abi1 libfftw3-bin libfftw3-dev libfftw3-doc ncurses-bin libncurses5 libncurses5-dev libncurses5-dbg libfontconfig1-dev libxrender-dev libpulse-dev swig g++ automake autoconf libtool python-dev libfftw3-dev libcppunit-dev libboost-all-dev libusb-dev libusb-1.0-0-dev fort77 libsdl1.2-dev python-wxgtk3.0 git libqt4-dev python-numpy ccache python-opengl libgsl-dev python-cheetah python-mako python-lxml doxygen qt4-default qt4-dev-tools libusb-1.0-0-dev libqwtplot3d-qt5-dev pyqt4-dev-tools python-qwt5-qt4 cmake git wget libxi-dev gtk2-engines-pixbuf r-base-dev python-tk liborc-0.4-0 liborc-0.4-dev libasound2-dev python-gtk2 libzmq3-dev libzmq5 python-requests python-sphinx libcomedi-dev python-zmq libqwt-dev libqwt6abi1 python-six libgps-dev libgps23 gpsd gpsd-clients python-gps python-setuptools python3-numpy python3-scipy python3-matplotlib aircrack-ng python3-pyqt5 gcc-arm-none-eabi

echo "INSTALLING UHD"
echo "=============================\n"
sudo -u $USRNM mkdir $IPATH/workarea-uhd
sudo -u $USRNM git clone https://github.com/EttusResearch/uhd $IPATH/workarea-uhd/

(cd $IPATH/workarea-uhd/;
sudo -u $USRNM git checkout $UHDV;
sudo -u $USRNM mkdir $IPATH/workarea-uhd/host/build)

(cd $IPATH/workarea-uhd/host/build/;
sudo -u $USRNM cmake ../;
sudo -u $USRNM make -j4;
sudo -u $USRNM make test;
sudo make install;
sudo ldconfig
sleep 1
sudo -u $USRNM uhd_find_devices
sleep 5
sudo /usr/local/lib/uhd/utils/uhd_images_downloader.py)

echo "INSTALLING GNURADIO"
echo "=============================\n"
sudo -u $USRNM mkdir $IPATH/workarea-gnuradio
sudo -u $USRNM git clone --recursive https://github.com/gnuradio/gnuradio $IPATH/workarea-gnuradio/

(cd $IPATH/workarea-gnuradio/;
sudo -u $USRNM git checkout $GNURADV;
sudo -u $USRNM git submodule update --init --recursive;
sudo -u $USRNM mkdir build;)

(cd $IPATH/workarea-gnuradio/build;
sudo -u $USRNM cmake ../;
sudo -u $USRNM make -j4;
sudo -u $USRNM make test;
sudo make install;
sudo ldconfig;)

sudo -u $USRNM gnuradio-config-info --version
sudo -u $USRNM gnuradio-config-info --prefix
sudo -u $USRNM gnuradio-config-info --enabled-components

echo "CONFG. USB DRIVERS"
echo "=============================\n"
(cd $IPATH/workarea-uhd/host/utils;
pwd
sudo cp uhd-usrp.rules /etc/udev/rules.d/;
sudo udevadm control --reload-rules;
sudo udevadm trigger;
echo "retriggering drivers";
sleep 30;)

echo "INSTALL GR-EVENTSTREAM"
echo "=============================\n"
(cd $IPATH;
sudo -u $USRNM git clone https://github.com/osh/gr-eventstream.git;)
(cd $IPATH/gr-eventstream;
sudo -u $USRNM mkdir build;)
(cd $IPATH/gr-eventstream/build;
sudo -u $USRNM cmake ../;
sudo -u $USRNM make -j4;
sudo make install;
sudo ldconfig;)

echo "INSTALL HACKRF LIBS"
echo "=============================\n"
sudo apt-get install build-essential cmake libusb-1.0-0-dev pkg-config libfftw3-dev
(cd $IPATH;
sudo -u $USRNM git clone https://github.com/mossmann/hackrf.git;)
(cd $IPATH/hackrf/host;
sudo -u $USRNM mkdir build;)
(cd $IPATH/hackrf/host/build;
sudo -u $USRNM cmake ../;
sudo -u $USRNM make -j4;
sudo make install;
sudo ldconfig;
sudo udevadm control --reload-rules;
sudo udevadm trigger;
echo "retriggering drivers";
sleep 30;)

echo "BUILD OSMOSDR GR LIBS"
echo "=============================\n"
(cd $IPATH;
sudo -u $USRNM git clone https://github.com/osmocom/gr-osmosdr.git;)
(cd $IPATH/gr-osmosdr;
sudo -u $USRNM mkdir build;)
(cd $IPATH/gr-osmosdr/build;
sudo -u $USRNM cmake ../;
sudo -u $USRNM make -j4;
sudo make install;
sudo ldconfig;)

echo "INSTALL GQRX"
echo "=============================\n"
sudo apt-get install -y gqrx-sdr

echo "INSTALL INSPECTRUM"
echo "=============================\n"
sudo apt-get install -y inspectrum

echo "=============================\n"
echo "DONE WITH INSTALLATION"
echo "PLEASE RUN THE FM RADIO TEST WITH USRP AND GNURADIO"
echo "=============================\n"
